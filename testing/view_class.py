#!/usr/bin/env python3


def view_class():
    """ View the students for a specified class. """
    # Connect to the database
    connection = sqlite3.connect("studentinfo.db")
    cursor = connection.cursor()

    # Create the SQL command that will be used to query the database
    sql= ("SELECT Firstname, Lastname FROM studentTbl WHERE Class_name = ?")

    # For each option, set the class_name to the chosen class
    if var1.get() == 1:
        class_name = '6D'
    
    elif var2.get() == 1:
        class_name = '6S'
    
    elif var3.get() == 1:
        class_name = '5GH'
    
    elif var4.get() == 1:
        class_name = '5H'
    
    elif var5.get() == 1:
        class_name = '5B'

    # Run the SQL command with the chosen class name
    cursor.execute(sql, [(class_name)])

    # Fetch the output from the SQL command
    # classlist becomes a list of tuples like: [('Adam', 'Cook'), ('Charlie', 'Josey')]
    classlist = cursor.fetchall()

    # This is just to test:
    classlist = [('Adam', 'Cook'), ('Charlie', 'Josey')]

    # This is where the fun begins...
    # STEP ONE: Get a list of STRINGS like "FirstName LastName"
    # For each tuple in the classlist, the first name is tuple[0] and the last name is tuple[1].
    full_name_list = list()
    for name_group in classlist:
        # I.e. for every tuple in classlist... make a full name

        # The below code joins all elements in name_group ('Adam', 'Cook') together with a space (' ')
        # ('Adam', 'Cook') ==> 'Adam Cook'
        full_name = ' '.join(name_group)

        # Now that we have converted the tuple in to a full name, we need to store it so we can use it later:
        full_name_list.append(full_name)

    # STEP TWO: After the for loop has ended, all of the full names will be in the full_name_list.
    # What we need to do, is make something that looks like this: "Adam Cook\nCharlie Josey"
    # Using join, this will turn ['Adam Cook', 'Charlie Josey'] in to the above ^^^^
    full_class_string = '\n'.join(full_name_list)

    # Try putting in the newly formed "Adam Cook\nCharlie Josey" string in to the below function:
    # See what happens: (Hopefully this works - I haven't actually tested it)
    tkinter.messagebox.showinfo("Class {}".format(class_name), full_class_string)
