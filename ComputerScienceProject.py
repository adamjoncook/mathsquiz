from tkinter import *
from tkinter import ttk
import tkinter.messagebox
import tkinter.simpledialog
import sqlite3
import crypt
import random

##import hashlib
##def hashPassword(password):
##    Hashed = hashlib.md5(password.encode())
##    return Hashed.hexdigest()

TEACHER_CODE = ("secret")

#Hash password, store hash in database instead of password
def hashPassword(password):
    Hashed = crypt.crypt(password,crypt.METHOD_SHA256)
    return Hashed 

##def ClassTable():
##    connection = sqlite3.connect("classinfo.db")
##    cursor = connection.cursor()
##    try:
##        sql_table = """
##            CREATE TABLE classTbl(
##            Class_name string,
##            Teacher_name string,
##            No_of_students integer,
##            primary key(Class_name)
##            )"""
##        cursor.execute(sql_table)
##        print("classTbl created in classinfo.db")
##        connection.commit()
##        connection.close()
##    except:
##        pass


 
def QuestionTable():
    connection = sqlite3.connect("questions.db")
    cursor = connection.cursor()
    try:
        sql_table = """
            CREATE TABLE questionsTbl(
            Question string,
            Answer integer,
            Difficulty string,
            Topic string,
            primary key(Question)
            )"""
        cursor.execute(sql_table)
        connection.commit()
        connection.close()
    except:
        pass


def TeacherTable():
    connection = sqlite3.connect("teacherinfo.db")
    cursor = connection.cursor()
    try:
        sql_table = """
            CREATE TABLE teacherTbl(
            Username string,
            Password string,
            Code string,
            primary key (Username)
            )"""
        cursor.execute(sql_table)
        connection.commit()
        connection.close()
    except:
        pass

        
#creates a new table in the database, username is the primary key
def StudentTable():
    connection = sqlite3.connect("studentinfo.db")
    cursor = connection.cursor()
    try:
        sql_table = """
            CREATE TABLE studentTbl(
            Firstname string,
            Lastname string,
            Username string,
            Password string,
            Class_name string,
            primary key (Username)
            )"""
        cursor.execute(sql_table)
        connection.commit()
        connection.close()
    except:
        pass

def SetQuizTable():
    connection = sqlite3.connect("set_questions.db")
    cursor = connection.cursor()
    try:
        sql_table = """
            CREATE TABLE set_questionsTbl(
            unique_id integer,
            QuizNumber integer,
            Class_name string,
            Question varchar(255),
            Answer integer,
            primary key(unique_id)
            )"""
        cursor.execute(sql_table)
        connection.commit()
        connection.close()
    except:
        pass

def studentViewClass():
    connection = sqlite3.connect("studentinfo.db")
    cursor = connection.cursor()
    uname = username_entry.get()
    # Create the SQL command that will be used to query the database
    classId =  cursor.execute("SELECT Class_name FROM studentTbl WHERE Username = (?)", (uname))
    
    #try:
        # Run the SQL command with the chosen class name
    cursor.execute("SELECT Firstname, Lastname FROM studentTbl WHERE Class_name = (?)", (classId))
        # Fetch the output from the SQL command and cast to a dictionary object
    classlist = cursor.fetchall()
        #Get a list of strings like "FirstName LastName"
    full_name_list = list()
    for name_group in classlist:
            #Joins all elements in name_group together with a space (' ')
            # ('fname', 'lname') ==> 'fname lname'
        full_name = ' '.join(name_group)
            # Now that we have converted the tuple in to a full name, we need to store it so we can use it later:
        full_name_list.append(full_name)
        #join turns ['fname lname', 'fname lname'] ==> fname lname\nfname lname
    full_class_string = '\n'.join(full_name_list)
    tkinter.messagebox.showinfo("Class {}".format(class_name), full_class_string)
    connection.close()
    #except:
         #pass
    
def checkAnswer(a):
    Correct_answers = quizAnswers
    print(Correct_answers)
#    inputtedAnswer = [a]
#    if inputtedAnswer == Correct_answers[0]:
#        print(inputtedAnswer, Correct_answers[0])
   
    

def AttemptQuiz():
    connection = sqlite3.connect("studentinfo.db")
    cursor = connection.cursor()
    user = username_entry.get()
    uniqueQuizNumber = quiz_number.get()
  
    sql = ("SELECT Class_name FROM studentTbl WHERE Username = ?")
    cursor.execute(sql, [(user)])
    Class_Id = cursor.fetchall()
    classname_list = list()
    for group in Class_Id:
        ID = ' '.join(group)
        classname_list.append(ID)
        full_class_ID = '\n'.join(classname_list)
        
    
    connection.close()

    connection = sqlite3.connect("set_questions.db")
    cursor = connection.cursor()
    cursor.execute("SELECT Question FROM set_questionsTbl WHERE Question is not null AND Class_name = (?) AND QuizNumber = (?)", (full_class_ID, uniqueQuizNumber))
    questions = cursor.fetchall()
    if questions == []:
        ErrorMessage = Label(screen10,text = "Incorrect quiz number given. Please try again. ")
        ErrorMessage.grid(row=3, column=0)
    else:
        screen8= Tk()
        screen8.withdraw()   
        for q in questions:
            a = tkinter.simpledialog.askfloat("Question", str(q), parent = screen8)
            checkAnswer(a)
        
    connection.close()    

def AttemptQuizScreen():
    screen7.withdraw()
    global screen10
    screen10 = Tk()
    screen10.geometry("325x200")
    screen10.resizable(False,False)
    screen10.title("Quiz")
    screen10.configure(background = "Light blue")

    frame_heading = Frame(screen10)
    frame_heading.grid(row=0, column=0, columnspan=2, padx=30, pady =5)
    Label(frame_heading, text = "Enter quiz number provided by teacher: ").grid(row=0, column=0)
    global quiz_number
    quiz_number = Entry(frame_heading, width =15, bg = "white")
    quiz_number.grid(row=1, column=0)

    attemptButton = Button(screen10, text = "Attempt", width = 10, command = test)
    attemptButton.grid(row=1, column =0, padx=10, pady=5)

    
    
def LogoutStudent():
    screen.deiconify()
    screen7.withdraw()
    
def studentLoginMenu():
    screen.withdraw()
    global screen7
    screen7 = Tk()
    screen7.geometry("315x200")
    screen7.resizable(False,False)
    screen7.title("Student Menu")
    screen7.configure(background = "Light blue")

    frame_heading = Frame(screen7)
    frame_heading.grid(row=0, column=0, columnspan=3, padx=30, pady =5)
    Label(frame_heading, text = "Work set: ", font =('Arial bold', 15)).grid(row=0, column=2)

    AttemptQuizButton = Button(screen7, text="Attempt quiz", width=13, command = AttemptQuizScreen)
    AttemptQuizButton.grid(row=1, column=0, padx=25, pady=5)
    viewClassButton = Button(screen7, text="View your class", width=13, command = studentViewClass)
    viewClassButton.grid(row=1, column=3, padx=10, pady=5)
    logoutButton = Button(screen7, text = "Logout", width = 13, command = LogoutStudent)
    logoutButton.grid(row =2, column = 0, padx=10, pady=5)
    
def login():
    connection = sqlite3.connect("studentinfo.db")
    cursor = connection.cursor()

    errorMessage = Label(screen,width = 30)
    errorMessage.grid(row =4 , column = 0, columnspan = 2, padx = 5, pady = 5)

    cursor.execute('SELECT Username, Password FROM studentTbl')
    all_rows = dict(cursor.fetchall())
    #global StudentUname
    StudentUname = username_entry.get()

    try:
        pw = all_rows[StudentUname]
    except KeyError:
        # Username does not exist
        errorMessage.config(text = "User doesn't exist")
        return None
        

    if pw == hashPassword(password_entry.get()):
        studentLoginMenu()
    else:
       errorMessage.config(text = "Incorrect password")
       return


#adds inputted user details into the database
def register():    
    StudentTable()
    
    FirstName = Firstname.get()
    LastName = Lastname.get()
    UserName = Username.get()
    PassWord = Password.get()
    ClassID = Class_ID.get()
    try:
        errorMessage = Label(screen1,width = 30)
        errorMessage.grid(row =5 , column = 0, columnspan = 2, padx = 5, pady = 5)
        if len(PassWord)<6:
            errorMessage.config(text = "Password must be at east 6 characters")
            Username.delete(0, 'end')
            Password.delete(0, 'end')
            Username.focus_set()
        else:
            errorMessage.config(text = "Password accepted")
            connection = sqlite3.connect("studentinfo.db")
            cursor = connection.cursor()
            myRec = []
            myRec.extend([FirstName, LastName, UserName, hashPassword(PassWord), ClassID])
            cursor.execute("INSERT INTO studentTbl VALUES (?,?,?,?,?)", myRec)
            connection.commit()
            print("User details accepted")
            connection.close()

    except:
        errorMessage.config(text = "User already exists")
        Username.delete(0, 'end')
        Password.delete(0, 'end')
        Username.focus_set()
    
    
        

    
def backStudent():
    screen.deiconify()
    screen1.withdraw()
    
#creates window for when registration button is pressed
def register_screen():
    global screen1
    screen.withdraw()
    screen1 = Tk()
    screen1.geometry("330x354")
    screen1.resizable(False,False)
    screen1.title("Student registration")
    screen1.configure(background = "Light blue")

    frame_heading = Frame(screen1)
    frame_heading.grid(row=0, column=0, columnspan = 2, padx = 30, pady = 5)
    Label(frame_heading, text = "Enter details below: ", font = ('Arial bold', 16)).grid(row=0, column=0, padx=0, pady=5)

    frame_entry = Frame(screen1)
    frame_entry.grid(row=1, column=0, columnspan=2, padx = 25, pady = 10)
    Label(frame_entry, text = "First name: ").grid(row=0, column=0, padx=10 , pady=10)
    Label(frame_entry, text = "Last name: ").grid(row=1, column=0, padx=10, pady=10)
    Label(frame_entry, text = "Username: ").grid(row=2, column=0, padx=10, pady=10)
    Label(frame_entry, text = "Password: ").grid(row=3, column=0, padx=10, pady=10)
    Label(frame_entry, text = "Class ID: ").grid(row=4, column=0, padx=10, pady=10)


    global Firstname
    global Lastname
    global Username
    global Password
    global Class_ID
    Firstname = Entry(frame_entry, width = 15, bg = "white")
    Firstname.grid(row=0, column=1, padx=5, pady = 5)   
    Lastname = Entry(frame_entry, width = 15, bg = "white")
    Lastname.grid(row=1, column=1, padx=5, pady = 5)    
    Username = Entry(frame_entry, width = 15, bg = "white")
    Username.grid(row=2, column=1, padx=5, pady = 5)    
    Password= Entry(frame_entry, width = 15, bg = "white", show = "*")
    Password.grid(row=3, column=1, padx=5, pady = 5)
    Class_ID = ttk.Combobox(frame_entry,text = "", width =15)
    Class_ID.grid(row=4, column=1,padx=5, pady=5)
    Class_ID.config(values = ("6D","6S","5GH","5H","5B"))
    
    register_button = Button(screen1, text="Register", width=7, command = register)
    register_button.grid(row=4, column=1, padx=0, pady=5)

    back_button = Button(screen1, text = "Back", width =7, command = backStudent)
    back_button.grid(row=4, column=0, padx=0, pady=5)

def submitQuestion():
    connection = sqlite3.connect("questions.db")
    cursor = connection.cursor()
    QuestionTable()
    Message = Label(screen4,width = 20, text = "")
    Message.grid(row=5,column=1)
    
    Question = question.get()
    CorrectAnswer = correctAnswer.get()
    Difficulty = difficulty.get()
    QuestionTopic = questionTopic.get()
    myRec = []
    
    try:
        myRec.extend([Question, CorrectAnswer, Difficulty, QuestionTopic])
        cursor.execute("INSERT INTO questionsTbl VALUES (?,?,?,?)", myRec)
        connection.commit()
        connection.close()
        Message.configure(text = "Equation added")
    except:
        Message.configure(text="Question already exists")

def clear():
    Message = Label(screen4,width = 20, text = "")
    Message.grid(row=5,column=1)
    try:
        question.delete(0, 'end')
        correctAnswer.delete(0, 'end')
        difficulty.delete(0, 'end')
        questionTopic.delete(0, 'end')
        question.focus_set()
        Message.config(text = "Cleared")
    except:
        pass
    
def backQuestion():
    screen3.deiconify()
    screen4.withdraw()
    
def createQuestion():
    global screen4
    screen3.withdraw()
    screen4 = Tk()
    screen4.geometry("450x300")
    screen4.title("Question entry")
    screen4.resizable (False, False)
    screen4.configure(background = "Light blue")
    frame_entry = Frame(screen4)
    frame_entry.grid(row=1,column=0,columnspan=2,padx = 25, pady = 10)
   

    
    Label(frame_entry, text ="Enter the question: ").grid(row=0,column=0,padx=10,pady=5)
    Label(frame_entry, text ="Correct answer: ") .grid(row=1,column=0,padx=10,pady=5)
    Label(frame_entry, text ="Difficulty: ").grid(row=2, column=0, padx=10, pady=5)
    Label(frame_entry, text ="Topic: ").grid(row=3, column=0, padx =10, pady=5)

    
    global question
    question= Entry(frame_entry, width = 30, bg = 'white', font = ('Arial', 11))
    question.grid(row = 0, column = 1,padx = 5,pady = 5)
    global correctAnswer
    correctAnswer = Entry(frame_entry, width = 30 , bg = 'white', font = ('Arial', 11))
    correctAnswer.grid(row = 1, column = 1, padx= 5,pady = 5)
    global difficulty
    difficulty = ttk.Combobox(frame_entry, text="", font =('Arial', 11))
    difficulty.grid(row=2, column=1, padx=5, pady=5)
    difficulty.config(values = ("Easy", "Medium", "Hard"))
    global questionTopic
    questionTopic = ttk.Combobox(frame_entry, text ="",font =('Arial', 11))
    questionTopic.grid(row=3, column=1, padx =5, pady=5)
    questionTopic.config(values = ("Addition", "Subtraction", "Multiplication", "Division", \
                                   "Algebra", "Decimals"))
    


    submit_button = Button(screen4,text="Submit",width=7,command=submitQuestion)
    submit_button.grid(row=2,column=0,padx=0,pady=5)

    clear_button = Button(screen4,text= "Clear",width=7,command=clear)
    clear_button.grid(row=2,column=1,padx=0,pady=5)

    backButton = Button(screen4, text = "Back", width =7, command = backQuestion)
    backButton.grid(row=3, column =0, padx=0, pady=5)

def setQuiz():
    try:
        SetQuizTable()
    except:
        pass
    #connect to database with questions
    connection = sqlite3.connect("questions.db")
    cursor = connection.cursor()
    #command to get questions and answers from database
    sql = ('SELECT Question, Answer FROM questionsTbl WHERE Difficulty = ?')

    global quizAnswers
    quizQuestions = []
    quizAnswers = []
    quizQuestionsEasy = []
    quizQuestionsMed = []
    quizQuestionsHard = []

    #gets 10 random 'easy' questions
    easyQuestions = cursor.execute(sql, [('Easy')])
    all_questionsEasy = cursor.fetchall()
    randomOrderEasy = random.sample(all_questionsEasy, k=10)
    quizQuestionsEasy.extend(randomOrderEasy)
    #print(randomOrderEasy)

    #gets 10 random 'medium' questions
    medQuestions = cursor.execute(sql, [('Medium')])
    all_questionsMed = cursor.fetchall()
    randomOrderMed = random.sample(all_questionsMed, k=10)
    quizQuestionsMed.extend(randomOrderMed)
    #print(randomOrderMed)

    #gets 10 random 'hard' questions
    hardQuestions = cursor.execute(sql, [('Hard')])
    all_questionsHard = cursor.fetchall()
    randomOrderHard = random.sample(all_questionsHard, k=10)
    quizQuestionsHard.extend(randomOrderHard)
    #print(randomOrderHard)
    connection.close()

    #connect to database
    
    connection = sqlite3.connect("set_questions.db")
    cursor = connection.cursor()

    quizClass = QuizClassEntry.get()
    quizNumber = QuizNumberEntry.get()
    #cursor.execute("INSERT INTO set_questionsTbl (Class_name, QuizNumber) VALUES (?,?)", (quizClass, quizNumber))
 

    #seperates question and answers from each difficulty
    a,b = zip(*quizQuestionsEasy)
    c,d = zip(*quizQuestionsMed)
    e,f = zip(*quizQuestionsHard)

    #all questions stored together
    zipped_questions = (a + c + e)
    #all answers stored together
    zipped_answers = (b + d + f)
    #appending questions and answers to list
    quizQuestions.extend(zipped_questions)
    quizAnswers.extend(zipped_answers)
    num = QuizNumberEntry.get()
    #get this to input all questions(a,c,e) and answers(b,d,e) into database
    cursor.execute("SELECT unique_id FROM set_questionsTbl WHERE QuizNumber = (?)",(num))
    ID = cursor.fetchall()
    for answer in quizAnswers:
        cursor.execute("UPDATE set_questionsTbl SET Answer = (?) WHERE unique_id = (?)",(answer,i))
        
##    for question in quizQuestions:
##        cursor.execute( "INSERT INTO set_questionsTbl (QuizNumber, Class_name, Question) VALUES (?,?,?)", (quizNumber,quizClass,question))
##    
##    for answer in quizAnswers:
##        cursor.execute("UPDATE set_questionsTbl SET Answer = (?) WHERE QuizNumber =(?) AND unique_id =(?) ", (answer, QuizNumberEntry, i))
  
    
    connection.commit()
    connection.close()
    SetMessage = Label(screen9,width = 20, text = "Quiz set")
    SetMessage.grid(row=7,column=1)
    
    
def backQuiz():
    screen3.deiconify()
    screen9.withdraw()
    
def setQuizScreen():
    global screen9
    screen3.withdraw()
    screen9 = Tk()
    screen9.geometry("280x200")
    screen9.title("SetQuiz")
    screen9.resizable (False, False)
    screen9.configure(background = "Light blue")
    frame_entry = Frame(screen9)
    frame_entry.grid(row=1,column=0,columnspan=2,padx = 25, pady = 10)
    Label(frame_entry, text = "Assign quiz number: ").grid(row=0,column=0,padx=2,pady=5)
    Label(frame_entry, text = "Select class assign: ").grid(row=1, column=0, padx=5, pady=5)
    
    #Get teacher to input quiz number and which class. Then attempt quiz should pull quiz for class that student is in. Primary key = quiz number
    global QuizClassEntry
    global QuizNumberEntry
    QuizClassEntry = ttk.Combobox(frame_entry,text="",  width = 5)
    QuizClassEntry.grid(row=1, column=2, padx=5, pady=5)
    QuizClassEntry.config(values = ("6D", "6S", "5GH", "5G", "5B"))
    QuizNumberEntry = Entry(frame_entry, width =6, bg ="white")
    QuizNumberEntry.grid(row =0, column =2, padx=5, pady=5)

    setButton = Button(screen9, text = "Set", width =6, command = setQuiz)
    setButton.grid(row=3, column=0, padx=5, pady=5)

    backButton = Button(screen9, text = "Back", width =6, command = backQuiz)
    backButton.grid(row=3, column=1, padx=5, pady=5)
   
    

def backEdit():
    screen3.deiconify()
    screen6.withdraw()
    
def deleteEdit():
    connection = sqlite3.connect("studentinfo.db")
    cursor = connection.cursor()
    Message = Label(screen6,width = 20, text = "")
    Message.grid(row=5,column=1)
    
    classSelected = ClassSelect.get()
    firstEdit = FirstnameEdit.get()
    lastEdit = LastnameEdit.get()
    myRec =[]   
    myRec.extend([firstEdit, lastEdit, classSelected])
    try:
        cursor.execute("DELETE FROM studentTbl  WHERE (Firstname, Lastname, Class_name) = (?,?,?)", myRec)
        Message.config(text = firstEdit +" "+ lastEdit +" deleted")
    except:
        Message.config(text = "Error")
        FirstnameEdit.delete(0, 'end')
        LastnameEdit.delete(0, 'end')
        ClassSelect.focus_set()

    connection.commit()
    connection.close()
    
def addEdit():
    connection = sqlite3.connect("studentinfo.db")
    cursor = connection.cursor()
    Message = Label(screen6,width = 20, text = "")
    Message.grid(row=5,column=1)
    
    classSelected = ClassSelect.get()
    firstEdit = FirstnameEdit.get()
    lastEdit = LastnameEdit.get()
    myRec =[]   
    myRec.extend([firstEdit, lastEdit, classSelected])
    try:
        cursor.execute("INSERT INTO studentTbl (Firstname, Lastname, Class_name) VALUES (?,?,?)", myRec)
        Message.config(text = firstEdit +" "+ lastEdit +" added")
    
        connection.commit()
        connection.close()
    except:
        Message.config(text = "Error")
        ClassSelect.delete()
        FirstnameEdit.delete()
        LastnameEdit.delete()
        ClassSelect.focus_set()
  
    
def SelectClassEdit():
    global screen6
    screen3.withdraw()
    screen6 = Toplevel(screen3)
    screen6.geometry("380x250")
    screen6.resizable(False, False)
    screen6.title("Edit class")
    screen6.configure(background = "Light blue")
    
    frame_entry = Frame(screen6)
    frame_entry.grid(row=1, column=0,columnspan =2, padx=15, pady=5)
    Label(frame_entry, text = "Select class to edit: ").grid(row=0,column=0,padx=2,pady=5)
    Label(frame_entry, text = "Input students firstname: ").grid(row=1, column=0, padx=5, pady=5)
    Label(frame_entry, text = "Input students lastname: ").grid(row=2, column=0, padx=5, pady=5)

    global ClassSelect
    ClassSelect = ttk.Combobox(frame_entry,text="",  width = 15)
    ClassSelect.grid(row=0, column=1, padx=5, pady=5)
    ClassSelect.config(values = ("6D", "6S", "5GH", "5G", "5B"))
    global FirstnameEdit
    FirstnameEdit = Entry(frame_entry, width =15, bg ="white")
    FirstnameEdit.grid(row =1, column =1, padx=5, pady=5)
    global LastnameEdit
    LastnameEdit = Entry(frame_entry,width = 15, bg="white")
    LastnameEdit.grid(row=2, column=1, padx=5, pady=5)

    addButton = Button(screen6, text = "Add", width =6, command = addEdit)
    addButton.grid(row=3, column=1, padx=5, pady=5)
    deleteButton = Button(screen6, text="Delete", width = 6, command = deleteEdit)
    deleteButton.grid(row=3, column=0, padx=5, pady=5)
    backButton = Button(screen6, text="Back", width = 5, command = backEdit)
    backButton.grid(row = 4, column = 0, padx=5, pady=5)
    

    

def view_class():
    connection = sqlite3.connect("studentinfo.db")
    cursor = connection.cursor()
    # Create the SQL command that will be used to query the database
    sql= ("SELECT Firstname, Lastname FROM studentTbl WHERE Class_name = ?")
    # For each option, set the class_name to the chosen class
    classSelect1 = ClassSelect.get()

    if classSelect1 == "6D":
        class_name = "6D"
    elif classSelect1 == "6S":
        class_name = "6S"
    elif classSelect1 == "5GH":
        class_name = "5GH"
    elif classSelect1 == "5H":
        class_name = "5H"
    elif classSelect1 == "5B":
        class_name = "5B"
    try:
        # Run the SQL command with the chosen class name
        cursor.execute(sql, [(class_name)])
        # Fetch the output from the SQL command and cast to a dictionary object
        classlist = cursor.fetchall()
        #Get a list of strings like "FirstName LastName"
        full_name_list = list()
        for name_group in classlist:
            #Joins all elements in name_group together with a space (' ')
            # ('fname', 'lname') ==> 'fname lname'
            full_name = ' '.join(name_group)
            # Now that we have converted the tuple in to a full name, we need to store it so we can use it later:
            full_name_list.append(full_name)
        #join turns ['fname lname', 'fname lname'] ==> fname lname\nfname lname
        full_class_string = '\n'.join(full_name_list)
        tkinter.messagebox.showinfo("Class {}".format(class_name), full_class_string)
        connection.close()
    except:
         pass

def backView():
    screen3.deiconify()
    screen5.withdraw()
    
def  SelectClassView():
    global screen5
    screen3.withdraw()
    screen5 = Tk()
    screen5.geometry("250x150")
    screen5.resizable(False, False)
    screen5.title("View class")
    screen5.configure(background = "Light blue")

    frame_entry = Frame(screen5)
    frame_entry.grid(row=0, column=0,columnspan =2, padx=30, pady=5)
    Label(frame_entry, text = "Select class to view: ").grid(row=0,column=0,padx=10,pady=5)

    global ClassSelect
    ClassSelect = ttk.Combobox(frame_entry, text ="",font =('Arial', 11))
    ClassSelect.grid(row=1, column=0, padx =5, pady=5)
    ClassSelect.config(values = ("6D","6S", "5GH", "5H", "5B"))

    Submit_Button = Button(screen5, text="Submit", width=7, command = view_class)
    Submit_Button.grid(row=2, column=1, padx=0, pady=5)

    backButton =  Button(screen5, text="Back", width = 7, command = backView)
    backButton.grid(row=2, column=0, padx=5, pady=5)
    
    
def LogoutTeacher():
    screen.deiconify()
    screen3.withdraw()

def TeacherMainMenu():
    screen2.withdraw()
    global screen3
    screen3 = Tk()
    screen3.geometry("315x200")
    screen3.resizable(False,False)
    screen3.title("Teacher Main Menu")
    screen3.configure(background = "Light blue")

    frame_heading = Frame(screen3)
    frame_heading.grid(row=0, column=0, columnspan=2, padx=30, pady =5)
    Label(frame_heading, text = "Work & Classes", font =('Arial bold', 15)).grid(row=0, column=3)
    
    CreateQuestionButton = Button(screen3, text="Create Questions", width=13, command = createQuestion)
    CreateQuestionButton.grid(row=1, column=2, padx=0, pady=5)
    SetQuizButton = Button(screen3, text="Set quiz", width=13, command = setQuizScreen)
    SetQuizButton.grid(row=2, column=2, padx=0, pady=5)
    ViewClassButton = Button(screen3, text = "View Classes", width =13, command = SelectClassView)
    ViewClassButton.grid(row =1, column = 0, padx=0, pady=5)
    EditClassButton = Button(screen3, text = "Edit Class", width =13, command = SelectClassEdit)
    EditClassButton.grid(row =2, column = 0, padx=0, pady=5)
##    ChangeCodeButton = Button(screen3, text = "Change Code", width=13, command = changeCodeScreen)
##    ChangeCodeButton.grid(row =3, column=2, padx=0, pady=5)
    
    backButton = Button(screen3, text = "Logout", font=('Arial bold',13), width =14, command = LogoutTeacher)
    backButton.grid(row =3, column = 0, padx=0, pady=5)
    

    
def TeacherRegister():
    connection = sqlite3.connect("teacherinfo.db")
    cursor = connection.cursor()

    TeacherTable()
    UserName = Username.get()
    PassWord = Password.get()
    Teacher_code = TeacherCode.get()
    try:
        errorMessage = Label(screen2,width = 30, text = "Password must be at least 6 characters")
        errorMessage.grid(row =5 , column = 0, columnspan = 2, padx = 5, pady = 5)
        if len(PassWord)<6 and Teacher_code == TEACHER_CODE:
            errorMessage.config(text = "Password must be at least 6 characters")
            Username.delete(0, 'end')
            Password.delete(0, 'end')
            TeacherCode.delete(0, 'end')
            Username.focus_set()
        elif len(PassWord)>=6 and Teacher_code != TEACHER_CODE:
            errorMessage.config(text = "Code incorrect")
            Username.delete(0, 'end')
            Password.delete(0, 'end')
            TeacherCode.delete(0, 'end')
            Username.focus_set()
        elif len(PassWord)>=6 and Teacher_code == TEACHER_CODE:
            errorMessage.config(text = "Details accepted")
            print("Username: ", UserName)
            print("Password: ", PassWord)
            print("Code: ", Teacher_code)

            connection = sqlite3.connect("teacherinfo.db")
            cursor = connection.cursor()
            myRec = []
            myRec.extend([UserName, hashPassword(PassWord), hashPassword(Teacher_code)])
            cursor.execute("INSERT INTO teacherTbl VALUES (?,?,?)", myRec)
            connection.commit()
            print("User details accepted")
            connection.close()
    except:
        errorMessage.config(text = "Username already exists")
        Username.delete(0, 'end')
        Password.delete(0, 'end')
        TeacherCode.delete(0, 'end')
        Username.focus_set()


def TeacherLogin():
    connection = sqlite3.connect("teacherinfo.db")
    cursor = connection.cursor()
    global errorMessageLogin
    errorMessageLogin = Label(screen2,width = 30, text = "")
    errorMessageLogin.grid(row =5 , column = 0, columnspan = 2, padx = 5, pady = 5)
    
    cursor.execute('SELECT Username, Password, Code FROM teacherTbl')
    all_rows = cursor.fetchall()
    uname = Username.get()
    pword = hashPassword(Password.get())
    code = hashPassword(TeacherCode.get())

    all_uname = list()
    for row in all_rows:
        all_uname.append(row[0])
        
    if uname not in all_uname:
        errorMessageLogin.config(text = "Username does not exist")
    else:
        for row in all_rows:
            if uname in row:
                if pword == row[1] and code == row[2]:
                    errorMessageLogin.config(text ="Login Successful")
                    connection.commit()
                    connection.close()
                    Username.delete(0, 'end')
                    Password.delete(0, 'end')
                    TeacherCode.delete(0, 'end')
                    Username.focus_set()
                    TeacherMainMenu() 
                else:
                    errorMessageLogin.config(text = "Password or code incorrect")
    
    
def backTeacher():
    screen.deiconify()
    screen2.withdraw()

def TeacherLoginMenu():
    screen.withdraw()
    global screen2
    screen2 = Tk()
    screen2.geometry("300x300")
    screen2.resizable(False,False)
    screen2.title("Teacher Menu")
    screen2.configure(background = "Light blue")
 
    frame_heading = Frame(screen2)
    frame_heading.grid(row=0, column=0, columnspan = 2, padx = 30, pady = 5)
    Label(frame_heading, text = "Enter details below: ", font = ('Arial bold', 16)).grid(row=0, column=0, padx=0, pady=5)

    frame_entry = Frame(screen2)
    frame_entry.grid(row=1, column=0, columnspan=2, padx = 25, pady = 10)
    Label(frame_entry, text = "Username: ").grid(row=0, column=0, padx=10 , pady=5)
    Label(frame_entry, text = "Password: ").grid(row=1, column=0, padx=10, pady=10)
    Label(frame_entry, text = "Code: ").grid(row=2, column=0, padx=10, pady=10)

    global Username
    global Password
    global TeacherCode
    Username = Entry(frame_entry, width = 15, bg = "white")
    Username.grid(row=0, column=1, padx=5, pady = 5)   
    Password = Entry(frame_entry, width = 15, bg = "white", show = "*")
    Password.grid(row=1, column=1, padx=5, pady = 5)    
    TeacherCode = Entry(frame_entry, width = 15, bg = "white", show = "*")
    TeacherCode.grid(row=2, column=1, padx=5, pady = 5)
    
    register_button = Button(screen2, text="Register", width=7, command = TeacherRegister)
    register_button.grid(row=2, column=1, padx=0, pady=5)

    login_button = Button(screen2, text="Login", width=7, command = TeacherLogin)
    login_button.grid(row=2, column=0, padx=0, pady=5)

    backButton = Button(screen2, text = "Back", width =7, command = backTeacher)
    backButton.grid(row =3, column = 0, padx=0, pady=5)


    
    
#main program

global screen
screen = Tk()
screen.geometry("300x250")
screen.resizable(False,False)
screen.title("Menu")
screen.configure(background = "Light blue")

frame_heading = Frame(screen)
frame_heading.grid(row=0, column=0, columnspan=2, padx = 30, pady = 5)
Label(frame_heading, text = "Login Menu", font = ('Arial bold', 16)).grid(row=0, column=0, padx=0, pady =5)

frame_entry = Frame(screen)
frame_entry.grid(row=1, column=0, columnspan=2, padx = 25, pady = 10)
Label(frame_entry, text = "Username: ").grid(row=0, column=0, padx=10 , pady=5)
Label(frame_entry, text = "Password: ").grid(row=1, column=0, padx=10, pady=10)

global username_entry
global password_entry
username_entry = Entry(frame_entry, width = 15, bg = "white")
username_entry.grid(row=0, column=1, padx=5, pady = 5)
password_entry = Entry(frame_entry, width = 15, bg = "white", show = "*")
password_entry.grid(row=1, column = 1,padx=5, pady=5)

login_button = Button(screen, text="Login", width=7, command = login)
login_button.grid(row=2, column=0, padx=0, pady=5)

newUser_button = Button(screen, text="New user", width=7, command = register_screen)
newUser_button.grid(row=2, column=1, padx=0, pady=5)

teacher_Menu = Button(screen, text= "Teacher", width =7, comman = TeacherLoginMenu)
teacher_Menu.grid(row=3, column=1, padx=0, pady=5)


screen.mainloop()


