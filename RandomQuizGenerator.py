import random

#The quiz data. Keys are questions, values are the answers.

answers = {'39 + 673' : '712' ,  '9/11 -  4/11' : '5/11' , '2 x 45' : '90' , '838 ÷ 1' : '838' , '99 ÷11' : '9' , '5 x 4 x 10' : '200' ,
           '7,064 - 502' : '6562' , '6^2 + 10' : '46' , '56.38 + 24.7' : '81.08' , ' ()- 10 = 298' : '308' , '270 ÷ 3' : '90' , '5,400 ÷ 9' : '600' ,
           '60 ÷ 15' : '4' , '5,776 - 855' : '4921' , '3,050,020 = 3,000,000 +()+ 20' : '50,000' , '10 - 5.4' : '4.6' , '5/7 + 3/21' : '6/7' ,
           '0.1 ÷ 100' : '0.001' , '3/4 of 1,000' : '750' , '785 x 23' : '18055' , '20% of 1,200' : '240' , '645÷43' : '15' , '0.5 x 28' : '14' ,
           '1/2 + 1/5' : '7/10' ,  '1 3/4 + 3/4' : '5/2' , '6 - 5.738' : '0.262' , '3.9 x 30' : '117' , '1 1/15 - 2/5' : '2/3' , '5413 x 86' : '465776' ,
           '99% of 200' : '198' , '1/4 ÷ 2' : '1/8' , '9**2 - 36 ÷ 9' : '77', '1.5 x 40' : '20' , '28% of 650' : '182' ,
           '4 2/3 - 1 6/7' : '59/21' , '8827 ÷ 97' : '91' }

for quizNum in range(35):
    #create quiz and answer key files
    quizFile = open('Mathsquiz%s.txt' % (quizNum + 1), 'w')
    answerKeyFile = open('Mathsquiz_answers%s.txt' % (quizNum + 1), 'w')

    #write out header for quiz
    quizFile.write('Name:                          Key: ()is a missing number from the equation\n\nDate:                          Key: ** is to the power of. E.g. 2**2 = 4 \n\n')
    quizFile.write(('  ' * 20)+ 'SAT Mathematical quiz(Form %s)' % (quizNum + 1))
    quizFile.write('\n\n')

    #shuffle order of questions
    questions = list(answers.keys())
    random.shuffle(questions)

    for questionNum in range(36):
        #get right and wrong answers
        correctAnswer = answers[questions[questionNum]]
        wrongAnswers = list(answers.values())
        del wrongAnswers[wrongAnswers.index(correctAnswer)]
        wrongAnswers = random.sample(wrongAnswers, 3)
        answerOptions = wrongAnswers + [correctAnswer]
        random.shuffle(answerOptions)

        #write the questions and answer options to the quiz file
        quizFile.write('%s. What is: %s?\n' % (questionNum + 1, questions[questionNum]))
        for i in range(4):
            quizFile.write('    %s. %s\n' % ('ABCD'[i], answerOptions[i]))
        quizFile.write('\n')

        #write the answer key to a file
        answerKeyFile.write('%s. %s\n' % (questionNum + 1, 'ABCD' [answerOptions.index(correctAnswer)]))


quizFile.close()
answerKeyFile.close()
