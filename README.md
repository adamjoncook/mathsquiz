# MathsQuiz

## Real-World problem
Great Torrignton Junior School's SAT results in mathematics have been below average for the past two years and they are in desperate need to improve their mathematical skills. The years 5 and 6 have had problems with their confidence in maths and by implementing a flexible mathematical resource, this will improve their confidence and therefore improve their mathematic skills.

## Solution
To create a resource/program that boosts students confidence and skills with mathematics.

## Criteria
- Above average performance: Group A equivalent algorithms. Almost excellent coding style characteristics and a highly effective Solution.
- Average performance: Group A equivalent algorithms. majority of code being excellent coding style characteristics and an effective solution.
-Below average performance: Group A equivalent algorithms. Some excellent coding style characteristics. Less than effective to fairly effective solution.

- Examples of group A algorithms:
  Cross-table parameterised SQL.
  Aggregate SQL functions.
  User/CASE-generated DDL script.
  Graph/Tree Traversal.
  List Operations.
  Linked list maintenance.
  Stack/Queue Operations.
  Hashing.
  Advanced matrix operations.
  Recursive algorithms.
  Mergesort or similar efficient sort.
  Dynamic generation of objects based on complex user-defines use of OOP model.
  Server-side scripting using using request and response objects and server-side extensions for a complex client-server model.
  Calling parameterised web service APIs and parsing JSON/XML to service a complex client-server model.


## Description
My Client teaches years 5 / 6. My program is aimed at the students my client teaches maths to. The aim of the program is to improve years 5 and 6’s mathematic skills and confidence. The students will either create an account or just submit a username for that quiz (similar to kerboodle). The program works by opening a file (maths quiz), that the students can see, given by the client along with a Q&A sheet the students cannot see which gets compared to the students answer. Once the student has submitted the answer to a question, the answer given is then written onto the Q&A sheet for comparison after the quiz. This Q&A worksheet will be in printable format for the students to take home. The quiz will have a limited amount of question and may have a maximum score.

Each quiz will either be for a specific category of maths or a maths quiz that covers all categories (Summary quiz). If an answer is answered correctly the student will score points which will be displayed. If the question is answered incorrectly they will not lose points (losing points may reduce confidence) however, instead, they will not score any points and be branched off to an easier question of the same subject which, If answered correctly, will score them points but less points than the original question. If this easier question is answered incorrectly, branch again or move on? If enough questions are answered correctly in a row e.g. 3/4, branch to a harder set of questions which give bonus points if answered correctly. If one of these is answered wrongly, branch to the original set of questions. An additional scoring method would be time. If the student answers correctly in for example the first 5 seconds -50 bonus points. 10 seconds – 25 bonus points. Anything longer than 10 seconds will not get bonus points.

At the end of the quiz the total score is stored in a database and presented on a leaderboard for the class to see. The leaderboard will provide the user name of the student, score, number of incorrect/ correct answers, quiz name and number, date/time.

The program will also provide a mathematical game or two. One of which will be pairs. This game will be played against A.I. and be taken in turns. Both the player and the A.I. will be able to see the oppositions turn with the position and card type of the card they picked. Humans don’t remember everything so an algorithm will be needed to be made to simulate a human, therefore it must not remember every position and card type of all cards that have been flipped. Another game type might be something similar to a quest. Each question progresses you further along the quest (maybe a map?) every 5th question may be a mini boss where two correct answers will need to be given to defeat it. An incorrect answer can cause the boss to heal? Final question may be a boss with high health level. Multiple questions will be needed to be answered correctly to defeat the boss. Again, incorrect answers will result in healing. Player may be able to choose character? Score of game will also be updated onto a leaderboard for the whole class. Top 3 may have podium.
